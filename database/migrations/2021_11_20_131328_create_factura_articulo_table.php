<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaArticuloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_articulo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('factura_id')
            ->nullable()
            ->constrained('facturas')
            ->cascadaOnUpdate()
            ->nullOnDelete();
            $table->foreignId('articulo_id')
            ->nullable()
            ->constrained('articulos')
            ->cascadaOnUpdate()
            ->nullOnDelete();
            $table->integer('precio');
            $table->integer('cantidad');
            $table->integer('total');
            $table->timestamps();

            //$table->foreignId('cliente_id')->constrained('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_articulo');
    }
}
