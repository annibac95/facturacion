<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cliente;

class ClientesController extends Controller
{
    public function _construc(){
        $this->middleware('auth');
    }
    
    public function index(){
        $clientes= Cliente::all();
       
        return view('admin.clientes.index', (compact('clientes')));
    }
    
    public function store(Request $request){
        $newCliente = new Cliente();
        $newCliente->nombre= $request->nombre;
        $newCliente->ruc= $request->ruc;
        $newCliente->telefono= $request->telefono;
        $newCliente->save();
        
        return redirect()->back();
    }

    public function update(Request $request, $clienteId){
        $cliente = Cliente::find($clienteId);
        $cliente->nombre= $request->nombre;
        $cliente->ruc= $request->ruc;
        $cliente->telefono= $request->telefono;
        $cliente->save();
        
        return redirect()->back();
    }

    public function delete(Request $request, $clienteId){
        $cliente = Cliente::find($clienteId);
        $cliente->delete();
        
        return redirect()->back();
    }


}