<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Factura;
use App\Models\Cliente;
use App\Models\Articulo;


class FacturasController extends Controller
{
    public function _construc(){
        $this->middleware('auth');
    }
    
    public function index(){
        $facturas= Factura::all();
        $clientes= Cliente::all();
        $articulos= Articulo::all();
        
       
        return view('admin.facturas.index', (compact('facturas','clientes','articulos')));
    }
    
    public function store(Request $request){
        $newFactura = new Factura();
        $newFactura->cliente_id= $request->cliente_id;
        $newFactura->fechahora= $request->fechahora;
        $newFactura->estado= $request->estado;
        $newFactura->save();
        
        return redirect()->back();
    }

    public function update(Request $request, $facturaId){
        $factura = Factura::find($facturaId);
        $factura->cliente_id= $request->cliente_id;
        $factura->fechahora= $request->fechahora;
        $factura->estado= $request->estado;
        $factura->save();
        
        return redirect()->back();
    }

    public function delete(Request $request, $facturaId){
        $factura = Factura::find($facturaId);
        $factura->delete();
        
        return redirect()->back();
    }

    public function detallefactura(Request $request, $facturaId){
        
        $factura= Factura::find($facturaId);
        
        return view('admin.facturasarticulos.index', compact('factura'));

    }

    
}
