<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Articulo;
use App\Models\Factura;
class FacturaArticuloController extends Controller
{
    //

    public function index (){
        $articulo= Articulo::find(1);
        $factura= Factura::find(3);
        return view('admin.facturasarticulos.index', compact('articulo','factura'));
    }
}
