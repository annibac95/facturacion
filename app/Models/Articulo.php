<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    use HasFactory;
    public function facturas(){
        return $this->belongsToMany('App\Models\Factura','factura_articulo');
    }

}
