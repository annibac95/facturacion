<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');   

Route::get('/admin/articulos', 'Admin\ArticulosController@index')->name('admin.articulos');
Route::post('/admin/articulos/create', 'Admin\ArticulosController@store')->name('admin.articulos.store');
Route::post('/admin/articulos/{articuloId}/update', 'Admin\ArticulosController@update')->name('admin.articulos.update');
Route::delete('/admin/articulos/{articuloId}/delete', 'Admin\ArticulosController@delete')->name('admin.articulos.delete');

Route::get('/admin/clientes', 'Admin\ClientesController@index')->name('admin.clientes');
Route::post('/admin/clientes/create', 'Admin\ClientesController@store')->name('admin.clientes.store');
Route::post('/admin/clientes/{clienteId}/update', 'Admin\ClientesController@update')->name('admin.clientes.update');
Route::delete('/admin/clientes/{clienteId}/delete', 'Admin\ClientesController@delete')->name('admin.clientes.delete');

Route::get('/admin/facturas', 'Admin\FacturasController@index')->name('admin.facturas');
Route::post('/admin/facturas/create', 'Admin\FacturasController@store')->name('admin.facturas.store');
Route::post('/admin/facturas/{facturaId}/update', 'Admin\FacturasController@update')->name('admin.facturas.update');
Route::delete('/admin/facturas/{facturaId}/delete', 'Admin\FacturasController@delete')->name('admin.facturas.delete');


Route::get('/admin/facturasarticulos/{facturaId}', 'Admin\FacturasController@detallefactura')->name('admin.facturasarticulos');

Auth::routes();