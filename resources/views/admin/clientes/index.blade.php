@extends('adminlte::page')

@section('title', 'Sistema de Facturación')

<!--@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">

@stop -->

@section('content_header')

<h1> Clientes </h1>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-create-clientes">
        Crear
    </button>
   

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listado de Clientes</h3>
                </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="clientes" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            
                            <th>Nombre</th>
                            <th>RUC</th>
                            <th>Teléfono</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clientes as $cliente)
                        <tr>
                            
                            <td>{{ $cliente->nombre }}</td>
                            <td>{{ $cliente->ruc }}</td>
                            <td>{{ $cliente->telefono}}</td>
                            <td>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-update-clientes-{{ $cliente->id }}">
                                Editar
                            </button>
                            <form action="{{route('admin.clientes.delete', $cliente->id)}}" method="POST"">
                                {{csrf_field()}}
                                @method('DELETE')
                                <button class="btn btn-danger">Eliminar</buttom>
                            </form>   
                            
                            </td>
                        </tr>
                             @include('admin.clientes.modal-update-clientes');
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Nombre</th>
                            <th>RUC</th>
                            <th>Teléfono</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

<!-- modal -->
<div class="modal fade" id="modal-create-clientes">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Crear Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.clientes.store')}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class='form-group'>
                        <label for="cliente">Nombre</label>
                        <input type="char" name="nombre" class="form-control" id="cliente">
                        <label for="ruc">RUC</label>
                        <input type="char" name="ruc" class="form-control" id="ruc">
                        <label for="telefono">Teléfono</label>
                        <input type="char" name="telefono" class="form-control" id="telefono">
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@stop

@section('js')
<script>
$(document).ready(function() {
    $('#clientes').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>
@stop
