@extends('adminlte::page')

@section('title', 'Sistema de Facturación')

@section('content_header')
<h1>Detalle Factura </h1>

@stop

@section('content')

<div class="container-fluid">
       <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Articulos en la factura</h3>
                    </div>
                    <table id="detallefactura" class="table table-bordered table-striped">
                        <thead>
                            <th>Articulo</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($factura->articulos as $detalle)
                            <tr>
                                <td>{{$detalle->nombre}}</td>
                                <td>{{$detalle->precio}}</td>
                                <td>{{$detalle->cantidad}}</td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
       </div>
   </div>

 


@stop

@section('js')
<script>
$(document).ready(function() {
    $('#detallefactura').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>
@stop