<!-- modal UPDATE articulo-->
                    
<div class="modal fade" id="modal-update-articulos-{{$articulo->id}}">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Actualizar Artículo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.articulos.update', $articulo->id)}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class='form-group'>
                        <label for="articulo">Descripción</label>
                        <input type="char" name="nombre" class="form-control" id="articulo" value="{{ $articulo->nombre}}">
                        <label for="precio">Precio</label>
                        <input type="integer" name="precio" class="form-control" id="precio" value="{{ $articulo->precio}}">
                        <label for="cantidad">Cantidad</label>
                        <input type="integer" name="cantidad" class="form-control" id="cantidad" value="{{ $articulo->cantidad}}">
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->