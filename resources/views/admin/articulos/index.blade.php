@extends('adminlte::page')

@section('title', 'Sistema de Facturación')

<!--@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">

@stop -->

@section('content_header')

<h1> Articulos </h1>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-create-articulos">
        Crear
    </button>
   

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listado de artículos</h3>
                </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="articulos" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            
                            <th>Descripción</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($articulos as $articulo)
                        <tr>
                            
                            <td>{{ $articulo->nombre }}</td>
                            <td>{{ $articulo->precio }}</td>
                            <td>{{ $articulo->cantidad}}</td>
                            <td>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-update-articulos-{{ $articulo->id }}">
                                Editar
                            </button>
                            <form action="{{route('admin.articulos.delete', $articulo->id)}}" method="POST"">
                                {{csrf_field()}}
                                @method('DELETE')
                                <button class="btn btn-danger">Eliminar</buttom>
                            </form>   
                            
                            </td>
                        </tr>
                             @include('admin.articulos.modal-update-articulos');
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Descripción</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

<!-- modal -->
<div class="modal fade" id="modal-create-articulos">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Crear Artículo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.articulos.store')}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class='form-group'>
                        <label for="articulo">Descripción</label>
                        <input type="char" name="nombre" class="form-control" id="articulo">
                        <label for="precio">Precio</label>
                        <input type="integer" name="precio" class="form-control" id="precio">
                        <label for="cantidad">Cantidad</label>
                        <input type="integer" name="cantidad" class="form-control" id="cantidad">
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@stop

@section('js')
<script>
$(document).ready(function() {
    $('#articulos').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>
@stop

