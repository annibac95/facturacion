@extends('adminlte::page')

@section('title', 'Sistema de Facturación')

<!--@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">

@stop -->

@section('content_header')

<h1> Facturas </h1>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-create-facturas">
        Crear
    </button>
   

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listado de facturas</h3>
                </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="facturas" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            
                            <th>ID</th>
                            <th>Cliente</th>
                            <th>Fecha y Hora</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($facturas as $factura)
                        
                        <tr>
                            
                            <td>{{ $factura->id }}</td>
                            <td>{{ $factura->cliente->nombre }}</td>
                            <td>{{ $factura->fechahora}}</td>
                            <td>{{ $factura->estado}}</td>
                            <td>
                            <div class="btn-group">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-update-facturas-{{ $factura->id }}">
                                Editar
                            </button>
                            </div> 
                            <div class="btn-group">
                            <form action="{{route('admin.facturas.delete', $factura->id)}}" method="POST"">
                                {{csrf_field()}}
                                @method('DELETE')
                                <button class="btn btn-danger">Borrar</button>
                            </form>   
                            </div>
                            <div class="btn-group">
                            <form action="{{route('admin.facturasarticulos', $factura->id)}}" >
                                <button class="btn btn-info "> Ver </buttom>
                            </form> 
                             </div>
                            
                        </tr>
                             @include('admin.facturas.modal-update-facturas');
                             
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Cliente</th>
                            <th>Fecha y Hora</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

<!-- modal -->
<div class="modal fade" id="modal-create-facturas">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Crear Factura</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.facturas.store')}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class='form-group'>
                        <label for="cliente_id">Cliente</label>
                        <select name="cliente_id" class="form-control" id="cliente_id">
                            <option value="">Elegir Cliente </option>
                            @foreach($clientes as $cliente)
                            <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
                            @endforeach
                        </select>



                        
                     </div>

                    <div class='form-group'>
                        <label for="fechahora">Fecha y Hora</label>
                        <input type="date" name="fechahora" class="form-control" id="fechahora">
                        <label for="estado">Estado</label>
                        <input type="char" name="estado" class="form-control" id="estado">
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@stop

@section('js')
<script>
$(document).ready(function() {
    $('#facturas').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>
@stop

